import mongoose from "mongoose";
import envVar from "../constant.js";

let connectToMongoDB = () => {
  mongoose.connect(envVar.dbUrl);
  console.log("App is connected to database successfully");
};

export default connectToMongoDB;
