import { config } from "dotenv";

config();

// export let secretKey = process.env.SECRET_KEY;
// export let user = process.env.USER;
// export let pass = process.env.PASS;

const envVar = {
  secretKey: process.env.SECRET_KEY,
  user: process.env.USER,
  pass: process.env.PASS,
  dbUrl: process.env.DB_URL,
  serverUrl: process.env.SERVER_URL,
};

export default envVar;
