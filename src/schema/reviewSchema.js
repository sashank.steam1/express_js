import { Schema } from "mongoose";

let reviewSchema = Schema({
  product: {
    type: Schema.ObjectId,
    ref: "Products",
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  user: {
    type: Schema.ObjectId,
    ref: "Users",
    required: true,
  },
});

export default reviewSchema;
