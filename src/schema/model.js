import { model } from "mongoose";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import teacherSchema from "./teacherSchema.js";
import reviewSchema from "./reviewSchema.js";
import adminSchema from "./adminSchema.js";
import collegeSchema from "./collegeSchema.js";

export let Products = model("Products", productSchema);
export let Users = model("Users", userSchema);
export let Teachers = model("Teachers", teacherSchema);
export let Reviews = model("Reviews", reviewSchema);
export let Admins = model("Admins", adminSchema);
export let Colleges = model("Colleges", collegeSchema);

//first letter capital
//variable name and model name must be same
