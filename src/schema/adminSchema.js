import { Schema } from "mongoose";

let adminSchema = Schema({
  name: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    minLength: [3, "name must be greater than 3 characters"],
    maxLength: [30, "name must be less than 30 characters"],
    // match: [/^[a-zA-Z\s]+$/, 'name must be only alphabet']
    validate: (value) => {
      let isValid = /^[a-zA-Z\s]+$/.test(value);
      if (!isValid) {
        let error = new Error("name must be only alphabet");
        throw error;
      }
    },
  },
  password: {
    type: String,
    required: true,
    validate: (value) => {
      let pattern =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,20})/;
      let isValid = pattern.test(value);
      // if (!isValid) {
      //    let error = new Error("password must be at least 8 character, max 20 character, must have atleast one number one uppercase one lowercase and one symbol");
      //   throw error
      // }

      let errorMsg;
      if (value.length < 8) {
        errorMsg = "password must be at least 8 character";
      } else if (value.length > 20) {
        errorMsg = "password must be less than 20 character";
      } else if (!/[a-z]/.test(value)) {
        errorMsg = "password must have at least one lowercase";
      } else if (!/[A-Z]/.test(value)) {
        errorMsg = "password must have at least one uppercase";
      } else if (!/[0-9]/.test(value)) {
        errorMsg = "password must have at least one number";
      } else if (!/[!@#$%^&*]/.test(value)) {
        errorMsg = "password must have at least one symbol";
      }

      let error = new Error(errorMsg);

      throw error;
    },
  },
  phoneNumber: {
    type: Number,
    required: true,
    lowercase: true,
    trim: true,
    validate: (value) => {
      // if (value < 1000000000 || value > 9999999999) {
      //   throw new Error("phone number is not valid");
      // }
      if (String(value).length !== 10) {
        let error = new Error("Phone number must be exact 10 digits");
        throw error;
      }
    },
  },
  roll: {
    type: Number,
    required: true,
    trim: true,
    min: [20, "roll must be greater than 20"],
    max: [30, "roll must be less than 30"],
  },
  isMarried: {
    type: Boolean,
    required: true,
    trim: true,
  },
  spouseName: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
  },
  gender: {
    type: String,
    required: true,
    trim: true,
    validate: (value) => {
      // if (value !== "male" && value !== "female" && value !== "other") {
      //   let error = new Error("gender must be male or female");
      //   throw error;
      // }
      if (value === "male" || value === "female" || value === "other") {
      } else {
        let error = new Error("gender must be either male, female or other");
        throw error;
      }
    },
  },
  dob: {
    type: Date,
    required: true,
    trim: true,
  },
  location: {
    country: {
      type: String,
    },
    exactLocation: {
      type: String,
    },
  },
  favTeacher: [
    {
      type: String,
    },
  ],
  favSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],
});

export default adminSchema;
