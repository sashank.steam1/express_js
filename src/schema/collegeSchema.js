import { Schema } from "mongoose";

let collegeSchema = Schema({
  name: {
    type: String,
  },
  address: {
    type: String,
  },
  collegeImage: {
    type: String,
  }
});

export default collegeSchema;
