import { Schema } from "mongoose";

let teacherSchema = Schema({
  fullName: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  profileImage: {
    type: String,
    required: false,
  },
});

export default teacherSchema;
