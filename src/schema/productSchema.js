import { Schema } from "mongoose";

let productSchema = Schema({
  name: {
    type: String,
    lowercase: true,
    //uppercase: true,
    trim: true, //removes spaces while inserting into db
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },
  isAvailable: {
    type: Boolean,
    default: true, //sends default value to db
    required: true,
  },
});

export default productSchema;
