import { Router } from "express";
import {
  createUser,
  deleteUser,
  loginUser,
  readAllUser,
  readSpecificUser,
  updateUser,
} from "../controller/userController.js";

let userRouter = Router();

userRouter.route("/").post(createUser).get(readAllUser);

userRouter.route("/login").post(loginUser);

userRouter //for specific data
  .route("/:id") //localhost:8000/user/any
  .get(readSpecificUser)
  .patch(updateUser)
  .delete(deleteUser);

export default userRouter;
