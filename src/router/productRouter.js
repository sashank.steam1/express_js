import { Router } from "express";
import { createProduct, deleteProduct, readAllProduct, readSpecificProduct, updateProduct } from "../controller/productController.js";

let productRouter = Router();

productRouter
  .route("/")
  .post(createProduct)
  .get(readAllProduct);

productRouter //for specific data
  .route("/:id") //localhost:8000/product/any
  .get(readSpecificProduct)
  .patch(updateProduct)
  .delete(deleteProduct);

export default productRouter;


