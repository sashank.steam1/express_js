import { Router } from "express";

const firstRouter = Router();

const getController1 = (req, res, next) => {
  console.log("Hello World!");
};

const getController2 = (a, b) => {
  return (req, res, next) => {
    console.log(a + b);
  };
};

firstRouter
  .route("/") // '/' means localhost:8000
  .post(
    (req, res, next) => {
      console.log("I am middleware 1");
      let error = new Error("error 1");
      next(error);
    },
    (err, req, res, next) => {
      console.log("I am 1 error middleware");
      next();
    },
    (req, res, next) => {
      console.log("I am middleware 2");
      next();
    },
    (req, res, next) => {
      console.log("I am middleware 3");
    }
  )
  //.get(getController1)
  .get(getController2(1, 2)) // use this if you want to pass params
  .patch(() => {
    console.log("Home Patch");
  })
  .delete(() => {
    console.log("Home Delete");
  });

firstRouter
  .route("/name")
  .post(() => {
    console.log("Name Post");
  })
  .get(() => {
    console.log("Name Get");
  })
  .patch(() => {
    console.log("Name Patch");
  })
  .delete(() => {
    console.log("Name Delete");
  });

export default firstRouter;

/* 
2 types of middleware:

  normal middleware: 
    (req, res, next) => {}
    to trigger next middleware we have to call next()

  error middleware:
    (err, req, res, next) => {}
    to trigger next middleware we have to call next(err)
*/
