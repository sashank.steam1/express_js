import { Router } from "express";
import {
  createCollege,
  deleteCollege,
  readAllCollege,
  readSpecificCollege,
  updateCollege,
} from "../controller/collegeController.js";
import upload from "../utils/upload.js";

let collegeRouter = Router();

collegeRouter
  .route("/")
  .post(upload.single("collegeImage"), createCollege)
  .get(readAllCollege);

collegeRouter //for specific data
  .route("/:id") //localhost:8000/college/any
  .get(readSpecificCollege)
  .patch(upload.single("collegeImage"), updateCollege)
  .delete(deleteCollege);

export default collegeRouter;
