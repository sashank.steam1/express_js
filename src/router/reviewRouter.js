import { Router } from "express";
import {
  createReview,
  deleteReview,
  readAllReview,
  readSpecificReview,
  updateReview,
} from "../controller/reviewController.js";

let reviewRouter = Router();

reviewRouter.route("/").post(createReview).get(readAllReview);

reviewRouter //for specific data
  .route("/:id") //localhost:8000/review/any
  .get(readSpecificReview)
  .patch(updateReview)
  .delete(deleteReview);

export default reviewRouter;

