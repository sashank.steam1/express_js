import { Router } from "express";

let traineeRouter = Router();

traineeRouter
  .route("/")
  .post((req, res, next) => {
    res.json({
      success: true,
      message: "Trainee created successfully.",
    });
  })
  .get((req, res, next) => {
    res.json({
      success: true,
      message: "Trainee read successfully.",
    });
  })
  .patch((req, res, next) => {
    res.json({
      success: true,
      message: "Trainee updated successfully.",
    });
  })
  .delete((req, res, next) => {
    res.json({
      success: true,
      message: "Trainee deleted successfully.",
    });
  });

export default traineeRouter;
