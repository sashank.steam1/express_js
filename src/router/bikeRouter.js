import { Router } from "express";

let bikeRouter = Router();

bikeRouter
  .route("/")
  .post((req, res, next) => {
    console.log(req.body);
    res.json("bike post");
  })
  .get((req, res, next) => {
    res.json("bike get");
  })
  .patch((req, res, next) => {
    console.log(req.query);
    res.json("bike patch");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  });

bikeRouter.route("/a/:id1/name/:id2").post((req, res, next) => {
  console.log(req.params); //req.params gives dynamic params as object
  res.json("hello");
});

export default bikeRouter;
