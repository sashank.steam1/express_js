import { Router } from "express";
import {
  createAdmin,
  deleteAdmin,
  readAllAdmin,
  readSpecificAdmin,
  updateAdmin,
} from "../controller/adminController.js";

let adminRouter = Router();

adminRouter.route("/").post(createAdmin).get(readAllAdmin);

adminRouter //for specific data
  .route("/:id") //localhost:8000/admin/any
  .get(readSpecificAdmin)
  .patch(updateAdmin)
  .delete(deleteAdmin);

export default adminRouter;
