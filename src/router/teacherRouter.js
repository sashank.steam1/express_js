import { Router } from "express";
import { createTeacher, deleteTeacher, readAllTeacher, readSpecificTeacher, updateTeacher } from "../controller/teacherController.js";

let teacherRouter = Router();

teacherRouter
  .route("/")
  .post(createTeacher)
  .get(readAllTeacher);

teacherRouter //for specific data
  .route("/:id") //localhost:8000/teacher/any
  .get(readSpecificTeacher)
  .patch(updateTeacher)
  .delete(deleteTeacher);

export default teacherRouter;

/* 
name => string => required
price => number => required
quantity => number => required
description => string => required
*/