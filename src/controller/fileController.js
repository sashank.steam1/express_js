import envVar from "../constant.js";

export const handleSingleFile = (req, res, next) => {
  let link = `${envVar.serverUrl}/${req.file.filename}`;
  res.json({
    success: true,
    message: "File uploaded successfully",
    result: link,
  });
};

export const handleMultipleFile = (req, res, next) => {
  /* let links = req.files.map(file => `http://localhost:8000/${file.filename}`);
  res.json({
    success: true,
    message: "Files uploaded successfully",
    result: links,
  }); */

  let links = req.files.map((item, i) => {
    return `${envVar.serverUrl}/${item.filename}`;
  });
  res.json({
    success: true,
    message: "Files uploaded successfully",
    result: links,
  });
  
  //remove space from file name
  // links = links.map((item) => {
  //   return item.replace(/\s/g, ''); // replace all whitespace with nothing
  // });
};
