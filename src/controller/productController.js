import { Products } from "../schema/model.js";

export let createProduct = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await Products.create(data); //insert data into db
    res.status(200).json({
      success: true,
      message: "Product created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllProduct = async (req, res, next) => {
  /* 
    - get data from db 
    - send data to postman
  */
  try {
    let result = await Products.find({}); //get data from db (gives all data from db)
    res.status(200).json({
      success: true,
      message: "Product read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificProduct = async (req, res, next) => {
  try {
    let result = await Products.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "Product read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateProduct = async (req, res, next) => {
  try {
    let result = await Products.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(400).json({
      success: true,
      message: "Product updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteProduct = async (req, res, next) => {
  try {
    let result = await Products.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "Product deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
