import { Admins } from "../schema/model.js";

export let createAdmin = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await Admins.create(data); //insert data into db
    res.status(200).json({
      success: true,
      message: "Admin created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllAdmin = async (req, res, next) => {
  /* 
    - get data from db 
    - send data to postman
  */
  // let { limit, page } = req.query;
  // let { sort, select, limit, page, myQuery } = req.query;

  try {
    let result = await Admins.find({}); //get data from db (gives all data from db in array of object)

    // result = await Admins.find(myQuery)
    //   .sort(sort)
    //   .select(select)
    //   .limit(limit)
    //   .skip((page - 1) * 2);

    // result = await Admins.find({})
    //   .limit(limit)
    //   .skip((page - limit) * 2);

    // result = await Admins.find({ fullName: "Nitan Thapa" }).select(
    //   "email fullName"
    // );

    // result = await Admins.find({ fullName: "Nitan Thapa" }).select(
    //   "-fullName -password"
    // );

    // result = await Admins.find({}).sort("name");

    // result = await Admins.find({}).sort("-name");
    
    res.status(200).json({
      success: true,
      message: "Admin read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificAdmin = async (req, res, next) => {
  try {
    let result = await Admins.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "Admin read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateAdmin = async (req, res, next) => {
  try {
    let result = await Admins.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "Admin updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteAdmin = async (req, res, next) => {
  try {
    let result = await Admins.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "Admin deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
