import { Reviews } from "../schema/model.js";

export let createReview = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await Reviews.create(data); //insert data into db
    res.status(200).json({
      success: true,
      result: result,
      message: "Review created successfully",
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllReview = async (req, res, next) => {
  /* 
    - get data from db 
    - send data to postman
  */
  try {
    let result = await Reviews.find({}).populate("product").populate("user"); //get data from db (gives all data from db)
    res.status(200).json({
      success: true,
      message: "Review read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificReview = async (req, res, next) => {
  try {
    let result = await Reviews.findById(req.params.id)
      .populate("product")
      .populate("user");
    res.status(200).json({
      success: true,
      message: "Review read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateReview = async (req, res, next) => {
  try {
    let result = await Reviews.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "Review updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteReview = async (req, res, next) => {
  try {
    let result = await Reviews.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "Review deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
