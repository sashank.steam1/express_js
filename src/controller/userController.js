import { Users } from "../schema/model.js";
import { sendEmail } from "../utils/sendMail.js";
import bcrypt from "bcrypt";

export const createUser = async (req, res, next) => {
  let data = req.body;
  try {
    data.password = await bcrypt.hash(data.password, 10);
    let result = await Users.create(data); //insert data into db

    await sendEmail({
      from: "Sashank Khadgi<sashank.khadgi00700@gmail.com>",
      to: data.email,
      subject: "User Registration",
      html: `
        <p>${data.fullName} thank you for registering.</p>
      `,
    });

    res.status(200).json({
      success: true,
      message: "User created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllUser = async (req, res, next) => {
  /* 
    - get data from db 
    - send data to postman
  */
  // let { limit, page } = req.query;
  let { sort, select, limit, page, myQuery } = req.query;

  try {
    let result = await Users.find({}); //get data from db (gives all data from db in array of object)

    result = await Users.find(myQuery)
      .sort(sort)
      .select(select)
      .limit(limit)
      .skip((page - 1) * 2);

    // result = await Users.find({})
    //   .limit(limit)
    //   .skip((page - limit) * 2);

    // result = await Users.find({ fullName: "Nitan Thapa" }).select(
    //   "email fullName"
    // );

    // result = await Users.find({ fullName: "Nitan Thapa" }).select(
    //   "-fullName -password"
    // );

    // result = await Users.find({}).sort("name");

    // result = await Users.find({}).sort("-name");

    res.status(200).json({
      success: true,
      message: "User read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readSpecificUser = async (req, res, next) => {
  try {
    let result = await Users.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "User read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const updateUser = async (req, res, next) => {
  try {
    let result = await Users.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "User updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteUser = async (req, res, next) => {
  try {
    let result = await Users.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "User deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const loginUser = async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;

  try {
    let user = await Users.findOne({ email: email });

    if (!user) {
      res.status(401).json({
        success: false,
        message: "Email not found",
      });
    } else {
      let dbPassword = user.password;
      let isValidPassword = await bcrypt.compare(password, dbPassword);

      if (isValidPassword) {
        res.status(200).json({
          success: true,
          message: "Logged in successfully",
        });
      } else {
        res.status(401).json({
          success: false,
          message: "Password incorrect",
        });
      }
    }
  } catch (error) {
    res.status(401).json({
      success: false,
      message: error.message,
    });
  }
};
