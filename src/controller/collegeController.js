import envVar from "../constant.js";
import { Colleges } from "../schema/model.js";

export const createCollege = async (req, res, next) => {
  let data = req.body;
  let link = `${envVar.serverUrl}/${req.file.filename}`;
  data.collegeImage = link;
  try {
    let result = await Colleges.create(data);
    res.status(200).json({
      success: true,
      message: "College created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readAllCollege = async (req, res, next) => {
  /* 
    - get data from db 
    - send data to postman
  */
  // let { limit, page } = req.query;
  let { sort, select, limit, page, myQuery } = req.query;

  try {
    let result = await Colleges.find({}); //get data from db (gives all data from db in array of object)

    // result = await Colleges.find(myQuery)
    //   .sort(sort)
    //   .select(select)
    //   .limit(limit)
    //   .skip((page - 1) * 2);

    // result = await Colleges.find({})
    //   .limit(limit)
    //   .skip((page - limit) * 2);

    // result = await Colleges.find({ fullName: "Nitan Thapa" }).select(
    //   "email fullName"
    // );

    // result = await Colleges.find({ fullName: "Nitan Thapa" }).select(
    //   "-fullName -password"
    // );

    // result = await Colleges.find({}).sort("name");

    // result = await Colleges.find({}).sort("-name");

    res.status(200).json({
      success: true,
      message: "College read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const readSpecificCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "College read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const updateCollege = async (req, res, next) => {
  try {
    let data = req.body;
    if (req.file !== undefined) {
      let link = `${envVar.serverUrl}/${req.file.filename}`;
      data.collegeImage = link;
    }
    let result = await Colleges.findByIdAndUpdate(req.params.id, data, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "College updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export const deleteCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "College deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
