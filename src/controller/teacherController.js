import { Teachers } from "../schema/model.js";

export let createTeacher = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await Teachers.create(data); //insert data into db
    res.status(200).json({
      success: true,
      message: "Teacher created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
}

export let readAllTeacher = async (req, res, next) => {
  /* 
    - get data from db 
    - send data to postman
  */
  try {
    let result = await Teachers.find({}); //get data from db (gives all data from db)
    res.status(200).json({
      success: true,
      message: "Teacher read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
}

export let readSpecificTeacher = async (req, res, next) => {
  try {
    let result = await Teachers.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "Teacher read successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
}

export let updateTeacher = async (req, res, next) => {
  try {
    let result = await Teachers.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.status(200).json({
      success: true,
      message: "Teacher updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
}

export let deleteTeacher = async (req, res, next) => {
  try {
    let result = await Teachers.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "Teacher deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
}