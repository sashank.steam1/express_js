import jwt from "jsonwebtoken";

/* //Generating token

let info = {
  id: "1234567890",
};

let secretKey = "dw14";

//expiryInfo must be a object with property expiresIn
let expiryInfo = {
  expiresIn: "1d",
};

let token = jwt.sign(info, secretkey, expiryInfo);

console.log(token); 

// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1Njc4OTAiLCJpYXQiOjE3MTQ3MDIzNjAsImV4cCI6MTcxNDc4ODc2MH0.pQRGbBI1wQGjbrULFZqksWKSUVEN7f0Vc6_ydf1mQB4
 */

//Verifying token

  /* 
    conditions to verify:
      - token must be made from given secret key
      - token must not be expired
  */

let secretKey = "dw14";

let token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMzQ1Njc4OTAiLCJpYXQiOjE3MTQ3MDIzNjAsImV4cCI6MTcxNDc4ODc2MH0.pQRGbBI1wQGjbrULFZqksWKSUVEN7f0Vc6_ydf1mQB4";

try {
  let info = jwt.verify(token, secretKey);
  console.log(info);
} catch (error) {
  console.log(error.message);
}
/* 
  if token is verified it gives info 
  if token is not verified it gives error
*/
