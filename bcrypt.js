import bcrypt from "bcrypt";

let ourPassword = "abc@1234";
let hashedPassword = await bcrypt.hash(ourPassword, 10);

console.log(hashedPassword);

let isMatch = await bcrypt.compare(ourPassword, hashedPassword);

console.log(isMatch);
