import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import traineeRouter from "./src/router/traineesRouter.js";
import productRouter from "./src/router/productRouter.js";
import connectToMongoDB from "./src/connectToDB/connectToMongoDB.js";
import teacherRouter from "./src/router/teacherRouter.js";
import userRouter from "./src/router/userRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import adminRouter from "./src/router/adminRouter.js";
import fileRouter from "./src/router/fileRouter.js";
import cors from "cors";
import { config } from "dotenv";
import collegeRouter from "./src/router/collegeRouter.js";
import swagerUi from "swagger-ui-express";
import YAML from "yamljs";
import path from "path"

config();

//making express application
let expressApp = express();
expressApp.use(cors());
expressApp.use(json());

const swaggerDocument = YAML.load(path.join("./public", "YAML.yaml"));
expressApp.use("/api-docs", swagerUi.serve, swagerUi.setup(swaggerDocument));

expressApp.use(express.static("./public"));
expressApp.use("/name", firstRouter); // localhost:8000/name
expressApp.use("/bike", bikeRouter);
expressApp.use("/trainee", traineeRouter);
expressApp.use("/product", productRouter);
expressApp.use("/teacher", teacherRouter);
expressApp.use("/user", userRouter);
expressApp.use("/review", reviewRouter);
expressApp.use("/admin", adminRouter);
expressApp.use("/file", fileRouter);
expressApp.use("/college", collegeRouter);

connectToMongoDB();

//attach port
expressApp.listen(8000, () => {
  console.log("Application is listening at port 8000");
});

/* 
make api:
  create router
  use that router to the main file(index.js)
*/
